package test;

// S1

public class B {
	
	private String param;
	private C b;
	
	B(){
		
	}
	public void x() {
		
	}
	
	public void y() {
		
	}

	public static void main(String[] args) {

	}

}

class A extends B{
	
	public void x() {	
		
	}
	public void y() {	
		
	}
	A(){
		
	}
}

class C{
	C(){
		
	}
	
}

class D{
	private B[] d;
	D(){
		
	}
	
}

class E{
	private B b;
	E(){
		
	}
	
}

class U{
	public void displayB(B b) {
		
	}
	U(){
		
	}
}